package servlets;

import org.json.JSONException;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author wenzhuo
 */
public class ServletUser extends javax.servlet.http.HttpServlet {
    /**
     * create a new user
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     * @see javax.servlet.ServletException
     * @see IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String prenom = request.getParameter("prenom");
        String nom = request.getParameter("nom");

        out.println(services.User.createUser(login,password,prenom,nom));
    }

    /**
     * consult information of an user by login
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     * @see javax.servlet.ServletException
     * @see IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String login = request.getParameter("login");
        try {
            out.println(services.User.getUser(token,login));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
