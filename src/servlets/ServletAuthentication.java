package servlets;

import org.json.JSONException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletAuthentication")
public class ServletAuthentication extends HttpServlet {
    /**
     * login with (login, password) or by checking token
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see ServletException
     * @see IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        try {
            //try with token if exists in client's Header
            if (request.getHeader("token") != null) {
                String token = request.getHeader("token");
                out.println(services.Authentication.loginWithToken(token));
            }else {
                //login by login & password
                //server will give the client a token
                String login = request.getParameter("login");
                String password = request.getParameter("password");
                out.println(services.Authentication.login(login, password));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * logout
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see ServletException
     * @see IOException
     */
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        try {
            out.println(services.Authentication.logout(token));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
