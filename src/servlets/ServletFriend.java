package servlets;

import org.json.JSONException;
import tools.ErrorJSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletFriend")
public class ServletFriend extends HttpServlet {
    /**
     * follow a person by his login
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String login = request.getParameter("login");
        try {
            out.println(services.Friend.addFollow(token, login));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * get the list of the users following (following - aboonnement)
     * or get the list of users followed (followers - abonné)
     * the credential is login
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String method = request.getParameter("method");
        String login = request.getParameter("login");
        if(method != null && !method.isEmpty()){
            try {
                if(method.equals("GET_FOLLOWING")){
                    out.println(services.Friend.getFollowing(token,login));
                }
                if (method.equals("GET_FOLLOWER")){
                    out.println(services.Friend.getFollower(token,login));
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            out.println(ErrorJSON.serviceRefused("Empty value method",400));
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String login = request.getParameter("login");
        if(login != null && !login.isEmpty()){
            try {
                out.println(services.Friend.deleteFollowing(token, login));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            out.println(ErrorJSON.serviceRefused("Empty value method",400));
        }
    }
}
