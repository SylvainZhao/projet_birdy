package servlets;

import org.json.JSONException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletComment")
public class ServletComment extends HttpServlet {
    /**
     * Post a comment to a message
     * token, comment and idMessage
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String idMessage = request.getParameter("idMessage");
        String comment = request.getParameter("comment");
        try{
            out.println(services.Comment.createComment(token,idMessage,comment));
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Get list of comments of a message
     * token and idMessage to fetch comments
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String idMessage = request.getParameter("idMessage");
        try{
            out.println(services.Comment.getComments(token, idMessage));
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * delete a comment by its _id in mongo db
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String idComment = request.getParameter("idComment");
        try{
            out.println(services.Comment.deleteComment(token, idComment));
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
