package servlets;

import org.json.JSONException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletMessage")
public class ServletMessage extends HttpServlet {
    /**
     * post a message
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see ServletException
     * @see IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String message = request.getParameter("message");
        try {
            out.println(services.Message.createMessage(token,message));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * get the messages of the user and his followings (to display in the front page /)
     * or, get messages of the user himself (use when consulting profile)
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see ServletException
     * @see IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        if(request.getParameter("login") == null){
            try {
                out.println(services.Message.getAllMessages(token));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            String login = request.getParameter("login");
            try{
                out.println(services.Message.getMessagesByLogin(token, login));
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Delete a message by it's object id in mongo db
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see ServletException
     * @see IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("charset=utf-8");
        PrintWriter out = response.getWriter();
        String token = request.getHeader("token");
        String idMessage = request.getParameter("idMessage");
        try {
            out.println(services.Message.deleteMessage(token,idMessage));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
