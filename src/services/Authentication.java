package services;

import io.jsonwebtoken.Claims;
import org.json.JSONException;
import org.json.JSONObject;
import tools.ErrorJSON;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class Authentication {

    /**
     * login with (login, password): check validity then insert into a new session into db
     * with id, a key generated and date
     * @param login login of user
     * @param password password of user
     * @return error in JSON if lack of arguments or wrong credentials; acceptation if (login, password) valid
     * @throws JSONException
     */
    public static JSONObject login(String login, String password) throws JSONException {
        //check null of login
        if(isBlank(login) || isBlank(password))
            return ErrorJSON.serviceRefused("Empty value", 400);

        if(!tools.Authentication.checkCredential(login, password))
            return ErrorJSON.serviceRefused("Wrong credential",400);

        //generate id, date, key
        String idUser = tools.Authentication.getIdUserByLogin(login);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(new Date().getTime());
        String key = tools.Authentication.generateToken(login, String.valueOf(idUser));

        return ErrorJSON.serviceAccepted("OK",200).put("token",key);
    }

    /**
     * check token is valid: check it is valid/expired/idUser/login is correct
     * @param token the token given by client
     * @return errorif token isn't valid; acceptation & a new token in JSON if valid
     * @throws JSONException
     */
    public static JSONObject loginWithToken(String token) throws JSONException {
        if(isBlank(token))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String new_token = tools.Authentication.updateToken(token);
            return ErrorJSON.serviceAccepted("OK",200).put("token",new_token);
        }else {
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    public static JSONObject logout(String token) throws JSONException {
        if(isBlank(token))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String logout_token = tools.Authentication.invalidateToken(token);
            return ErrorJSON.serviceAccepted("OK",200).put("token",logout_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

}
