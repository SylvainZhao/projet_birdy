package services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tools.ErrorJSON;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class Comment {
    /**
     * user creates a comment of message (idMessage)
     * @param token
     * @param idMessage
     * @param comment
     * @return
     * @throws JSONException
     */
    public static JSONObject createComment(String token, String idMessage, String comment) throws JSONException {
        if(isBlank(token)||isBlank(idMessage)||isBlank(comment))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getClaimsFromToken(token).getId();
            String new_token = tools.Authentication.updateToken(token);
            //Date now
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = df.format(new Date().getTime());
            tools.Comment.insertComment(idMessage, idUser, comment, date);
            return ErrorJSON.serviceAccepted("Created",201).put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }

    }

    public static JSONObject getComments(String token, String idMessage) throws JSONException{
        if(isBlank(token)||isBlank(idMessage))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String new_token = tools.Authentication.updateToken(token);
            JSONArray comments = tools.Comment.getComment(idMessage);
            return ErrorJSON.serviceAccepted("OK",200)
                    .put("comments",comments)
                    .put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    public static JSONObject deleteComment(String token, String idComment) throws JSONException {
        if(isBlank(token)||isBlank(idComment))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String new_token = tools.Authentication.updateToken(token);
            tools.Comment.deleteComment(idComment);
            return ErrorJSON.serviceAccepted("Deleted",204)
                    .put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }
}
