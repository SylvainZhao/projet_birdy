package services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tools.ErrorJSON;
import tools.User;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class Friend {
    /**
     * add a new follow (abonnement)
     * @param token
     * @param login
     * @return
     * @throws JSONException
     */
    public static JSONObject addFollow(String token, String login) throws JSONException {
        if(isBlank(token) || isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getClaimsFromToken(token).getId();
            String idUserToFollow = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            tools.Friend.insertFollow(idUser,idUserToFollow);
            return ErrorJSON.serviceAccepted("Created",201).put("token",new_token);
        }else {
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    /**
     * get the list of the users that you follow (following - aboonnement)
     * @param token token of user
     * @return
     */
    public static JSONObject getFollowing(String token, String login) throws JSONException {
        if(isBlank(token)||isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            JSONArray listFollowing = tools.Friend.getFollowing(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("following",listFollowing).
                    put("token",new_token);
        }else {
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    /**
     * get the list of the users who follow you (follower - abonné)
     * @param token token of user
     * @return
     */
    public static JSONObject getFollower(String token, String login) throws JSONException {
        if(isBlank(token)||isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            JSONArray listFollower = tools.Friend.getFollower(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("follower",listFollower)
                    .put("token",new_token);
        }else {
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    /**
     * Delete a following with pair(myID, following's ID)
     * @param token
     * @param login
     * @return
     * @throws JSONException
     */
    public static JSONObject deleteFollowing(String token, String login) throws JSONException{
        if(isBlank(token)||isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getClaimsFromToken(token).getId();
            String idUserFollow = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            tools.Friend.deleteFollow(idUser,idUserFollow);
            return ErrorJSON.serviceAccepted("Deleted",204).put("token",new_token);
        }else {
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }
}
