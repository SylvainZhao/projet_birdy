package services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tools.ErrorJSON;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * des services pour ServletUser
 * @author wenzhuo
 */
public class User {
    public static final String PW_PATTERN = "^(?![A-Za-z0-9]+$)(?![a-z0-9\\W]+$)(?![A-Za-z\\W]+$)(?![A-Z0-9\\W]+$)[a-zA-Z0-9\\W]{8,}$";

    public static JSONObject createUser(String login, String password, String nom, String prenom)  {
        //check null
        if(isBlank(login) || isBlank(password) || isBlank(nom) || isBlank(prenom))
            return ErrorJSON.serviceRefused("Empty value", 400);

        //check length limit
        if(login.length()>50 || password.length()>50 || nom.length()>50 || prenom.length()>50)
            return ErrorJSON.serviceRefused("Argument too long",400);

        //check password : 8 bits, a majuscule, a minuscule, number and special char
        if(!password.matches(PW_PATTERN))
            return ErrorJSON.serviceRefused("Password: at least 8 bits with an uppercase, a lowercase, a number and a special caracter",401);

        //check existence
        if(tools.User.userExists(login))
            return ErrorJSON.serviceRefused("User exists",409);

        tools.User.insertUser(login, password, nom, prenom);
        return ErrorJSON.serviceAccepted("Created",201);
    }

    public static JSONObject getUser(String token) throws JSONException {
        if(isBlank(token))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getClaimsFromToken(token).getId();
            String new_token = tools.Authentication.updateToken(token);
            JSONObject user = tools.User.getInfoUser(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("user",user).
                    put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    public static JSONObject getUser(String token, String login) throws JSONException{
        if(isBlank(token) || isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            JSONObject user = tools.User.getInfoUser(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("user",user).
                    put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }
}
