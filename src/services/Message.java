package services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tools.Authentication;
import tools.ErrorJSON;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;


public class Message {
    public static JSONObject createMessage(String token, String message) throws JSONException {
        if(isBlank(token) || isBlank(message))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getClaimsFromToken(token).getId();
            String new_token = tools.Authentication.updateToken(token);
            //Date now
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = df.format(new Date().getTime());
            tools.Message.insertMessage(idUser,message,date);
            return ErrorJSON.serviceAccepted("Created",201).put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }

    }

    /**
     * get all the messages of user and his followings
     * @param token
     * @return
     * @throws JSONException
     */
    public static JSONObject getAllMessages(String token) throws JSONException {
        if(isBlank(token))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = Authentication.getClaimsFromToken(token).getId();
            String new_token = tools.Authentication.updateToken(token);
            JSONArray messages = tools.Message.getMessageOfUsers(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("messages",messages).
                    put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }

    }

    /**
     * get messages of an user by his login
     * @param token
     * @param login
     * @return
     * @throws JSONException
     */
    public static JSONObject getMessagesByLogin(String token, String login) throws JSONException{
        if(isBlank(token)||isBlank(login))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String idUser = tools.Authentication.getIdUserByLogin(login);
            String new_token = tools.Authentication.updateToken(token);
            JSONArray messages = tools.Message.getMessageOfUser(idUser);
            return ErrorJSON.serviceAccepted("OK",200).
                    put("messages",messages).
                    put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }

    /**
     * delete a message by it's object id
     * @param token
     * @param idMessage
     * @return
     * @throws JSONException
     */
    public static JSONObject deleteMessage(String token, String idMessage) throws JSONException{
        if(isBlank(token) || isBlank(idMessage))
            return ErrorJSON.serviceRefused("Empty value", 400);
        //verify token
        if(tools.Authentication.verifyToken(token)){
            String new_token = tools.Authentication.updateToken(token);
            tools.Message.deleteMessage(idMessage);
            return ErrorJSON.serviceAccepted("Deleted",204).put("token",new_token);
        }else{
            return ErrorJSON.serviceRefused("Token invalid",400);
        }
    }
}
