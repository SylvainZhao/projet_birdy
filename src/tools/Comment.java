package tools;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import db.Database;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Comment {
    static MongoDatabase db = Database.getMongoDBConnection();
    static MongoCollection<Document> collection = db.getCollection("comments");

    /**
     * insert a comment into mongo db
     * @param idMessage the id of message to comment
     * @param idUser the user who posts a comment
     * @param comment the comment text
     * @param date date of comment
     */
    public static void insertComment(String idMessage, String idUser, String comment, String date) {
        Document query = new Document();
        query.append("idMessage", idMessage).append("idUser",idUser).append("comment",comment).append("date",date);
        collection.insertOne(query);

    }

    /**
     * get comments of a message by its idMessage in mongo db
     * @param idMessage
     * @return
     * @throws JSONException
     */
    public static JSONArray getComment(String idMessage) throws JSONException {
        List<JSONObject> comments = new ArrayList<>();
        Document query = new Document();
        query.append("idMessage",idMessage);
        MongoCursor<Document> cursor = collection.find(query).iterator();
        while(cursor.hasNext()){
            Document o = cursor.next();
            JSONObject obj = new JSONObject(o.toJson());
            JSONObject user = tools.User.getInfoUser(obj.getString("idUser"));
            obj.put("login",user.getString("login"));
            obj.put("nom",user.getString("nom"));
            obj.put("prenom",user.getString("prenom"));
            comments.add(obj);
        }

        Collections.sort(comments, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                String dateA = new String();
                String dateB = new String();
                try {
                    dateA = o1.getString("date");
                    dateB = o2.getString("date");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return dateB.compareTo(dateA);
            }
        });
        JSONArray res = new JSONArray(comments);
        return res;
    }

    public static void deleteComment(String idComment) {
        Document query = new Document("_id",new ObjectId(idComment));
        collection.deleteOne(query);
    }
}
