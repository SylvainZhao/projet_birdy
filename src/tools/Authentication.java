package tools;

import db.Database;
import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static java.util.Objects.requireNonNull;
import static tools.User.md5;

public class Authentication {

    private static final String SECRET = "rgsnsm#ldyh*ws%l&hdpmnmw@xyhndes";//secret key
    private static final Long TTL_EXPIRATION = 1000L * 60 * 30; //expiration 30 min
    private static final String ISSUER = "birdy";//issue from who : us, birdy

    /**
     * check credential of user
     * @param login login of user
     * @param password password of user
     * @return true if (login, password) is correct in database
     */
    public static boolean checkCredential(String login, String password){
        Connection c = null;
        boolean flag = false;
        try {
            c = Database.getMySQLConnection();
            String sql = "select * from user where login = ? and password = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, login);
            ps.setString(2, md5(password));
            ResultSet res = ps.executeQuery();
            if(res.next()){
                flag = true;
            }
            res.close();
            ps.close();
            c.close();
            return flag;
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * generate a token
     * @param login login of user
     * @param idUser id of user
     * @return String JWT
     */
    public static String generateToken(String login, String idUser) {

        SignatureAlgorithm signature = SignatureAlgorithm.HS256;

        byte[] secretBytes = DatatypeConverter.parseBase64Binary(SECRET);
        Key key = new SecretKeySpec(secretBytes, signature.getJcaName());

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Date expiration = new Date(nowMillis+TTL_EXPIRATION);
        JwtBuilder builder = Jwts.builder()
                .setId(idUser)
                .setSubject(login)
                .setIssuedAt(now)
                .setIssuer(ISSUER)
                .signWith(SignatureAlgorithm.HS512, key);
        if(!login.equals("admin_birdy")){
            builder.setExpiration(expiration);
        }
        return builder.compact();
    }

    /**
     * get claims from a token
     * @param token token to be decoded
     * @return Claims
     * @throws Exception
     */
    public static Claims getClaimsFromToken(String token) throws ExpiredJwtException {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET))
                .parseClaimsJws(token)
                .getBody();
        return claims;
    }

    /**
     * verify a token given by client
     * @param token JWT to be verified
     * @return true if this token is valid (idUser, login, expiration)
     */
    public static boolean verifyToken(String token) {
        Connection c = null;
        boolean flag = false;
        try {
            c = Database.getMySQLConnection();
            Claims claims = getClaimsFromToken(token);
            requireNonNull(claims.getId(), "Token id hasn't been found");
            requireNonNull(claims.getSubject(), "Token subject hasn't been found");
            requireNonNull(claims.getIssuedAt(), "Token creation date hasn't been found");

            String idUser = claims.getId();
            String login = claims.getSubject();

            String sql = "SELECT * FROM `user` WHERE idUser = ? AND login = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, idUser);
            ps.setString(2, login);
            ResultSet res = ps.executeQuery();
            if(res.next()){
                flag = true;
            }
            res.close();
            ps.close();
            c.close();

            return flag;
        }catch (SQLException sqle) {
            Database.manageException(sqle, c);
            return false;
        }catch (ExpiredJwtException e){
            return false;
        }
    }

    /**
     * recall the function generateToken(login,idUser) to generate a new token
     * @param token the old token
     * @return the new (latest) token
     */
    public static String updateToken(String token){
        Claims claims = tools.Authentication.getClaimsFromToken(token);
        String idUser = claims.getId();
        String login = claims.getSubject();
        String new_token = tools.Authentication.generateToken(login,idUser);
        return new_token;
    }

    /**
     * un invalid token with nothing inside
     * @return String of this invalid token
     */
    public static String invalidateToken(String token){
        SignatureAlgorithm signature = SignatureAlgorithm.HS256;
        byte[] secretBytes = DatatypeConverter.parseBase64Binary(SECRET);
        Key key = new SecretKeySpec(secretBytes, signature.getJcaName());

        Claims claims = tools.Authentication.getClaimsFromToken(token);
        String idUser = claims.getId();
        String login = claims.getSubject();

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Long QUICK_EXPIRATION = 1L;
        Date expiration = new Date(nowMillis+QUICK_EXPIRATION);
        JwtBuilder builder = Jwts.builder()
                .setId(idUser)
                .setSubject(login)
                .setIssuedAt(now)
                .setIssuer(ISSUER)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, key);
        return builder.compact();
    }


    public static String getIdUserByLogin(String login) {
        Connection c = null;
        String idUser = "";
        try {
            c = Database.getMySQLConnection();
            String sql = "select idUser from user where login = ? ";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, login);
            ResultSet res = ps.executeQuery();
            if(res.next()){
                idUser = res.getString("idUser");
            }
            res.close();
            ps.close();
            c.close();
            return idUser;
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        }
        return idUser;
    }
}
