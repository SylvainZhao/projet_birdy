package tools;

import org.json.JSONException;
import org.json.JSONObject;

public class ErrorJSON {
    public static JSONObject serviceRefused(String error, int status_code)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("error",error);
            jsonObject.put("status_code",status_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static JSONObject serviceAccepted(String error, int status_code)  {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("error",error);
            jsonObject.put("status_code",status_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
