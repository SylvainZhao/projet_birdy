package tools;

import db.Database;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * interroger la base de données : DAO
 * @author wenzhuo zhao
 */
public class User {

    /**
     * vérifier existence de login
     * @param login
     * @return true si login existe déjà
     */
    public static boolean userExists(String login){
        Connection c = null;
        boolean flag = false;
        try {
            c = Database.getMySQLConnection();
            String sql = "select * from user where login = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, login);
            ResultSet res = ps.executeQuery();
            if(res.next()) {
                flag = true;
            }
            res.close();
            ps.close();
            c.close();
            return flag;
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        }
        return false;
    }

    /**
     * insert into db a new user
     * @param login login of user
     * @param password password of user
     * @param prenom first name of user
     * @param nom last name of user
     */
    public static void insertUser(String login, String password, String prenom, String nom){
        Connection c = null;
        try {
            c = Database.getMySQLConnection();
            String sql = "insert into user (login, password, prenom, nom) values (?,?,?,?)";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,login);
            ps.setString(2,md5(password));
            ps.setString(3,prenom);
            ps.setString(4,nom);
            ps.executeUpdate();
            ps.close();
            c.close();
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    /**
     * get idUser from db by login
     * @param login login of user
     * @return
     */
    public static int getId(String login){
        Connection c = null;
        try {
            c = Database.getMySQLConnection();
            String sql = "select idUser from user where login = ? ";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, login);
            ResultSet res = ps.executeQuery();
            if(res.next()){
                int id = res.getInt("idUser");
                return id;
            }
            res.close();
            ps.close();
            c.close();
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        }
        return -1;
    }

    public static JSONObject getInfoUser(String idUser){
        JSONObject user = new JSONObject();
        Connection c = null;
        try {
            c = Database.getMySQLConnection();
            String sql = "select * from user where idUser = ? ";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, idUser);
            ResultSet res = ps.executeQuery();
            if(res.next()){
                String login = res.getString("login");
                String nom = res.getString("nom");
                String prenom = res.getString("prenom");

                user.put("login",login);
                user.put("nom",nom);
                user.put("prenom",prenom);
            }
            res.close();
            ps.close();
            c.close();
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public static String md5(String password) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("md5");
        byte[] cipherData = md5.digest(password.getBytes());
        StringBuilder builder = new StringBuilder();
        for(byte cipher : cipherData) {
            String toHexStr = Integer.toHexString(cipher & 0xff);
            builder.append(toHexStr.length() == 1 ? "0" + toHexStr : toHexStr);
        }
        return builder.toString();
    }




}
