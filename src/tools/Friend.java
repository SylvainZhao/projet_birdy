package tools;

import db.Database;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Friend {
    /**
     * insert a (idUser, idAbonne) pair into db
     * @param idUser the current user
     * @param idUserToFollow the user to follow
     */
    public static void insertFollow(String idUser, String idUserToFollow) {
        Connection c = null;
        try {
            c = db.Database.getMySQLConnection();
            String sql = "INSERT INTO friend (idUser,idAbonnement) values (?,?)";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,idUser);
            ps.setString(2,idUserToFollow);
            ps.executeUpdate();
            ps.close();
            c.close();

        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        }
    }

    /**
     * get the JSONArray of the users you follow (following - abonnement)
     * @param idUser idUser of me
     * @return JSONArray list of idUsers
     */
    public static JSONArray getFollowing(String idUser) {
        Connection c = null;
        List<JSONObject> listUserFollowing = new ArrayList<>();
        JSONArray result;
        try {
            c = db.Database.getMySQLConnection();
            String sql = "SELECT idAbonnement,login,nom,prenom FROM friend f, user u WHERE f.idUser = ? and f.idAbonnement = u.idUser ";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,idUser);
            ResultSet res = ps.executeQuery();
            JSONObject friend;
            while (res.next()){
                friend = new JSONObject();
                friend.put("idUser",res.getString("idAbonnement"));
                friend.put("login",res.getString("login"));
                friend.put("nom",res.getString("nom"));
                friend.put("prenom",res.getString("prenom"));
                listUserFollowing.add(friend);
            }
            ps.close();
            c.close();
            result = new JSONArray(listUserFollowing);
            return result;

        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        } catch(JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * get the JSONArray of the users who follow you (follower - abonné)
     * @param idUser idUser of me
     * @return JSONArray list of idUsers
     */
    public static JSONArray getFollower(String idUser) {
        Connection c = null;
        List<JSONObject> listIdUserFollower = new ArrayList<>();
        JSONArray result;
        try {
            c = db.Database.getMySQLConnection();
            String sql = "SELECT f.idUser, login, nom, prenom FROM friend f, user u WHERE idAbonnement = ? AND f.idUser = u.idUser";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,idUser);
            ResultSet res = ps.executeQuery();
            JSONObject friend;
            while (res.next()){
                friend = new JSONObject();
                friend.put("idUser",res.getString("idUser"));
                friend.put("login",res.getString("login"));
                friend.put("nom",res.getString("nom"));
                friend.put("prenom",res.getString("prenom"));
                listIdUserFollower.add(friend);
            }
            ps.close();
            c.close();
            result = new JSONArray(listIdUserFollower);
            return result;

        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete a following with pair(myID, following's ID)
     * @param idUser my id
     * @param idUserFollow following user's ID
     */
    public static void deleteFollow(String idUser, String idUserFollow) {
        Connection c = null;
        try {
            c = db.Database.getMySQLConnection();
            String sql = "DELETE FROM friend WHERE idUser = ? AND idAbonnement = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1,idUser);
            ps.setString(2,idUserFollow);
            ps.executeUpdate();
            ps.close();
            c.close();
        } catch (SQLException sqle) {
            Database.manageException(sqle, c);
        }
    }
}
