package tools;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import db.Database;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Message {
    static MongoDatabase db = Database.getMongoDBConnection();
    static MongoCollection<Document> collection = db.getCollection("messages");


    public static void insertMessage(String idUser, String message, String date){
        Document query = new Document();
        query.append("idUser", idUser).append("message",message).append("date",date);
        collection.insertOne(query);
    }

    /**
     * get all the messages (user himself and his followings) to display at /
     * sorted by date descendante
     * @param idUser
     * @return
     */
    public static JSONArray getMessageOfUsers(String idUser) throws JSONException {
        MongoCursor<Document> cursor;
        List<JSONObject> messages = new ArrayList<>();
        JSONArray following = tools.Friend.getFollowing(idUser);
        assert following != null;
        //messages of followings
        for(int i = 0; i < following.length(); i++){
            String id = String.valueOf(following.getJSONObject(i).getInt("idUser"));
            String login = following.getJSONObject(i).getString("login");
            String nom = following.getJSONObject(i).getString("nom");
            String prenom = following.getJSONObject(i).getString("prenom");
            Document query = new Document();
            query.append("idUser", id);
            cursor = collection.find(query).iterator();
            while (cursor.hasNext()){
                Document o = cursor.next();
                JSONObject obj = new JSONObject(o.toJson());
                obj.put("login",login);
                obj.put("nom",nom);
                obj.put("prenom",prenom);
                messages.add(obj);
            }
        }
        //messages of user himself
        JSONObject me = tools.User.getInfoUser(idUser);
        String login = me.getString("login");
        String nom = me.getString("nom");
        String prenom = me.getString("prenom");
        Document query = new Document();
        query.append("idUser", idUser);
        cursor = collection.find(query).iterator();
        while (cursor.hasNext()){
            Document o = cursor.next();
            JSONObject obj = new JSONObject(o.toJson());
            obj.put("login",login);
            obj.put("nom",nom);
            obj.put("prenom",prenom);
            messages.add(obj);
        }

        Collections.sort(messages, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                String dateA = new String();
                String dateB = new String();
                try {
                    dateA = o1.getString("date");
                    dateB = o2.getString("date");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return dateB.compareTo(dateA);
            }
        });
        JSONArray res = new JSONArray(messages);
        return res;
    }

    /**
     * get messages of an user by his id
     * sorted by date descendante
     * @param idUser
     * @return
     * @throws JSONException
     */
    public static JSONArray getMessageOfUser(String idUser) throws JSONException {
        JSONObject me = tools.User.getInfoUser(idUser);
        String login = me.getString("login");
        String nom = me.getString("nom");
        String prenom = me.getString("prenom");
        Document query = new Document();
        query.append("idUser",idUser);
        MongoCursor<Document> cursor = collection.find(query).iterator();
        List<JSONObject> messages = new ArrayList<>();
        while (cursor.hasNext()){
            Document o = cursor.next();
            JSONObject obj = new JSONObject(o.toJson());
            obj.put("login",login);
            obj.put("nom",nom);
            obj.put("prenom",prenom);
            messages.add(obj);
        }

        Collections.sort(messages, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                String dateA = new String();
                String dateB = new String();
                try {
                    dateA = o1.getString("date");
                    dateB = o2.getString("date");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return dateB.compareTo(dateA);
            }
        });
        JSONArray res = new JSONArray(messages);
        return res;
    }

    public static void deleteMessage(String idMessage) {
        Document query = new Document("_id",new ObjectId(idMessage));
        collection.deleteOne(query);
    }
}
