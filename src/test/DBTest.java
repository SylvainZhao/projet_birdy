package test;

import com.mongodb.client.MongoDatabase;
import db.Database;
import org.junit.Test;

import java.sql.*;

import static org.junit.Assert.assertTrue;

public class DBTest {

    @Test
    public void testMySQLConnection() throws SQLException {
        //in DBStatic.java, if pooling == true the context can't be initialized
        // without starting tomcat so the test can't pass in condition of pooling == true
        Connection c = db.Database.getMySQLConnection();
        assertTrue( c != null);
    }

    @Test
    public void testMongoConnection(){
        MongoDatabase db = Database.getMongoDBConnection();
        assertTrue( db != null );
    }


}
