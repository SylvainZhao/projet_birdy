package test;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import services.Message;

public class MessageTest {
    String token;
    @Before public void init(){
        token = tools.Authentication.generateToken("wenzhuo.zhao@etu.upmc.fr","3");
    }

    @Test
    public void postMessageTest() throws JSONException {
        services.Message.createMessage(token,"Just for a test");
    }

    @Test
    public void getMessageTest() throws JSONException {
        System.out.println(Message.getAllMessages(token));
        System.out.println(Message.getMessagesByLogin(token,"shihui.tan@etu.upmc.fr"));
    }

    @Test
    public void deleteMessageTest() throws JSONException {
        Message.deleteMessage(token,"5ebc80759b891e4223b8cbf6");
        System.out.println(Message.getAllMessages(token));
    }
}
