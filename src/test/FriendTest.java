package test;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

public class FriendTest {
    String token;
    @Before public void init(){
        token = tools.Authentication.generateToken("wenzhuo.zhao@etu.upmc.fr","3");
    }

    @Test public void getFollowing() throws JSONException {
        System.out.println("services.Friend.getFollowing(token) = " + services.Friend.getFollowing(token,"wenzhuo.zhao@etu.upmc.fr"));
    }

    @Test public void getFollower() throws JSONException {
        System.out.println("services.Friend.getFollower(token) = " + services.Friend.getFollower(token,"wenzhuo.zhao@etu.upmc.fr"));
    }
    
    @Test public void deleteFollowing() throws JSONException {
        //before deleting
        System.out.println("services.Friend.getFollowing(token) = " + services.Friend.getFollowing(token,"wenzhuo.zhao@etu.upmc.fr"));
        //delete
        System.out.println("services.Friend.deleteFollowing(token,\"zhen.hou@gmail.com\") = " + services.Friend.deleteFollowing(token,"zhen.hou@gmail.com"));
        //after delete
        System.out.println("services.Friend.getFollowing(token) = " + services.Friend.getFollowing(token,"wenzhuo.zhao@etu.upmc.fr"));
    }
}
