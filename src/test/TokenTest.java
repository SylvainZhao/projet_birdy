package test;

import io.jsonwebtoken.Claims;
import org.junit.Test;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class TokenTest {
    String login = "admin_birdy";
    String token = tools.Authentication.generateToken(login, "1");
    Claims claims = tools.Authentication.getClaimsFromToken(token);
    @Test
    public void testGenerateToken() throws Exception {
        System.out.println("token = " + token);
        System.out.println("claims.getId() = " + claims.getId());
        System.out.println("claims.setIssuer() = " + claims.getIssuer());
        System.out.println("claims.getSubject() = " + claims.getSubject());
        assertTrue(tools.Authentication.verifyToken(token));
    }
}
