package test;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
    String token;
    String login;
    @Before public void init(){
        token = tools.Authentication.generateToken("wenzhuo.zhao@etu.upmc.fr","3");
        login = "zhen.hou@gmail.com";
    }

    @Test
    public void checkProfileByLogin() throws JSONException {
        //except token, the two lines to print on console will be same
        System.out.println("tools.User.getInfoUser(\"2\") = " + tools.User.getInfoUser("2"));
        System.out.println("services.User.getUser(token, login) = " + services.User.getUser(token, login));
    }
}
