package test;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

public class CommentTest {
    String token;
    @Before public void init(){
        token = tools.Authentication.generateToken("wenzhuo.zhao@etu.upmc.fr","3");
    }

    @Test
    public void postComment() throws JSONException {
        services.Comment.createComment(token,"5ebd82e7e0affd665629f0fd","this is a comment");
    }
    
    @Test public void getComment() throws JSONException {
        System.out.println("services.Comment.getComments(token,\"5ebd82e7e0affd665629f0fd\") = " + services.Comment.getComments(token,"5ebd82e7e0affd665629f0fd"));
    }
}
